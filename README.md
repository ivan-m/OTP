CH/OTP Test Task
================

Understanding of task
---------------------

* There is a set of nodes $`N`$.

* Each node $`u \in N`$ sends messages to every other node $`v \in N,
  u \neq v`$ for a period of $`k`$ seconds.

* Messages consists of a deterministic random number $`n \in (0,
  1]`$ (i.e. using the same seed $`s`$ will result in the same
  sequence of messages)

    - It is assumed that no other communication is permitted within nodes.

* Each node stores the messages it receives in order that they are
  received (the 1-indexed sequence of such messages is denoted $`m`$
  and $`m_i`$ is the $`i^{th}`$ such message).

    - This includes the messages each node itself sends.

* After $`k`$ seconds, messages will no longer be sent/received.  Each
  node then has $`l`$ seconds to print the following tuple to stdout:

    ```math
    \left(|m|, \sum_{i=1}^{|m|} i \times m_i\right)
    ```
* Nothing else should be printed to stdout.

Technical considerations
------------------------

* To handle possible network failures, the `distributed-process-p2p`
  library will be used to for inter-node communication.

    - Time is required for all nodes to find each other.  As such, the
      expected runtime will be $`\approx 2 + s + l`$ seconds (assuming
      negligible delay for starting the entire set of nodes).

* As the output is ideally reproducible, we cannot somehow try and
  involve a time component within the messages to ensure ordering
  (e.g. an encoding of time-based UUIDs).

* As the only messages nodes are permitted to send to each other are
  the deterministic random numbers, each node must rely solely upon
  the messages it receives.

    - As such it is possible for a specific node to not receive a
      particular message.

    - In particular, as each node must also record its own messages,
      that is likely to be "received" by it before it receives a
      message from another node that was actually sent earlier.  As
      such, the list of messages for each node will differ.

* The actual sequence of received messages is not required, only the
  count and overall sequence-based sum is.

Running locally
---------------

The `otp-local.sh` script will run a few nodes locally to test the
solution.  The `node_ports` variable can be used to control how many
instances are run.

For example:

```bash
OTP $./otp-local.sh --send-for 4 --wait-for 2 --with-seed 10
(55,936.646132339846)
(55,936.646132339846)
(55,936.646132339846)
(56,989.6913213992019)
```

Debugging output can be found in `otp-local-err.*`.

Running manually
----------------

If you don't want to use the `otp-local.sh` script (e.g. you're not
actually going to run it locally but across multiple machines), then:

1. `stack build` to build the executable
2. The resulting executable can now be found in
   `$(stack path --local-install-root)/otp`
   (referred to as just `otp` from here on).
3. `otp --help`

Typically, you would start just one node:

```bash
otp --send-for 4 --wait-for 2 --with-seed 10 $host1 $port1
```

then launch each other node referring to that first one:

```bash
otp --send-for 4 --wait-for 2 --with-seed 10 $host2 $port2 $host1:$port2
```

(more than one other node can be listed at the end).

Limitations
-----------

* With the peer-to-peer nature, this solution tries to dynamically
  find which nodes are available to send messages to each time; it
  appears that even running locally these do not always succeed.

    - Interestingly, with the four node sample used in `otp-local.sh`,
      there are three typical scenarios:

        1) All four achieve the same result

        2) Two pairs of two (typically 1+4 and 2+3)

        3) The first node receives messages, the rest don't.

        More debugging would be needed to resolve this.

    - For a production system it would be worth comparing the
      difference between caching the peers (which would have less
      resilience due to sending messages to peers that are unable to
      receive them or possibly missing some nodes that were late to
      register) vs the current approach (which would require more
      network traffic and has the current unfortunate behaviour of not
      always succeeding).

    - Trying to obtain the list of peered nodes for debugging purposes
      seems to hang the program.

* No defaults are provided for the command-line flags.

* The current `Message`/`match` based implementation is meant to
  provide more guarantees for time ordering than using
  `nsend`/`expect` but is much slower (~50 messages sent vs ~8000).
