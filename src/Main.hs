{-# LANGUAGE AutoDeriveTypeable, DeriveAnyClass, DeriveGeneric, RecordWildCards
             #-}

{- |
   Module      : Main
   Description : CH/OTP Test Task Main
   Copyright   : (c) Ivan Lazar Miljenovic
   License     : AllRightsReserved
   Maintainer  : Ivan.Miljenovic@gmail.com



 -}

module Main (main) where


import qualified Control.Distributed.Backend.P2P as P2P

import Control.Distributed.Process.Extras.Time  (seconds)
import Control.Distributed.Process.Extras.Timer (exitAfter, sleep)
import Control.Distributed.Process.Lifted       (Process, catchExit, forward,
                                                 getSelfPid, match, nsend,
                                                 receiveWait, register, say,
                                                 spawnLocal, unregister,
                                                 wrapMessage)
import Control.Distributed.Process.Lifted.Class (liftP)
import Control.Distributed.Process.Node.Lifted  (initRemoteTable)
import Network.Socket                           (HostName, ServiceName)

import System.Random (StdGen, mkStdGen, random)

import Control.Applicative (many, (<$>), (<*>))
import Data.Monoid         ((<>))
import Options.Applicative (ParserInfo, auto, execParser, fullDesc, help,
                            helper, info, long, metavar, option, strArgument)

import Control.Monad              (forM_, forever)
import Control.Monad.State.Strict (StateT, evalStateT, execStateT, gets, modify)
import Control.Monad.Trans        (liftIO)
import Data.Bifunctor             (first)
import Data.Binary                (Binary)
import GHC.Generics               (Generic)

--------------------------------------------------------------------------------

main :: IO ()
main = execParser parseOptions >>= runNode

data Options = Options
  { sendFor    :: !Int
  , waitFor    :: !Int
  , withSeed   :: !Int
  , nodeHost   :: !HostName
  , nodePort   :: !ServiceName
  , knownNodes :: ![String]
  } deriving (Eq, Show, Read)

parseOptions :: ParserInfo Options
parseOptions = info (helper <*> prs) fullDesc
  where
    prs = Options <$> parseSendFor
                  <*> parseWaitFor
                  <*> parseWithSeed
                  <*> parseNodeHost
                  <*> parseNodePort
                  <*> parseKnownNodes

    parseSendFor = option auto (long "send-for"
                                <> help "How many seconds should the system send messages?")

    parseWaitFor = option auto (long "wait-for"
                                <> help "Grace period in seconds for printing results")

    parseWithSeed = option auto (long "with-seed"
                                 <> help "Integral seed for deterministic RNG")

    parseNodeHost = strArgument (metavar "addr" <> help "Host for this node")

    parseNodePort = strArgument (metavar "port" <> help "Port for this node")

    parseKnownNodes = many (strArgument (metavar "<node>"
                                         <> help "Known nodes in host:port format"))

--------------------------------------------------------------------------------

runNode :: Options -> IO ()
runNode opts@Options{..} =
  P2P.bootstrap nodeHost
                nodePort
                ext
                initRemoteTable
                (map P2P.makeNodeId knownNodes)
                (nodeProcess opts)
  where
    ext = const (nodeHost, nodePort)

nodeProcess :: Options -> Process ()
nodeProcess opts@Options{..} = do
  -- Launch receiving thread in case some nodes start early
  rp <- spawnLocal receiveProcess

  sleep (seconds 2) -- Give the dispatcher 2 second to discover other
                    -- nodes

  -- Now that we should be fully initialised, kill the receiving
  -- process after an appropriate period of time has gone by.
  _ <- exitAfter (seconds (sendFor + waitFor)) rp ExitReceive

  -- Launch the sending process.
  sp <- spawnLocal (sendProcess opts)
  _ <- exitAfter (seconds sendFor) sp ExitSend

  -- Need to wait until both sub-processes are complete.
  sleep (seconds (sendFor + waitFor))

type Message = Double

data ExitReceive = ExitReceive
  deriving (Show, Generic, Binary)

data ExitSend = ExitSend
  deriving (Show, Generic, Binary)

messageReceivingService :: String
messageReceivingService = "otp-messages"

--------------------------------------------------------------------------------

receiveProcess :: Process ()
receiveProcess = do
  getSelfPid >>= register messageReceivingService

  ReceiveState{..} <- execStateT go initReceiveState
  say "All messages received."

  -- Don't receive any future messages
  unregister messageReceivingService

  liftIO (print (messageCount, messageWeightedSum))
  where
    go :: StateT ReceiveState Process ()
    go = do
      mMsg <- receiveWait [match return]
      say ("Have a message! " ++ show mMsg)

      forM_ (mMsg :: Maybe Message) $ \msg -> do
        say ("Received message of " ++ show msg)
        modify (addMessage msg)
        go

    addMessage :: Message -> ReceiveState -> ReceiveState
    addMessage msg rs = rs { messageCount = c
                           , messageWeightedSum = messageWeightedSum rs
                                                  + fromIntegral c * msg
                           }
      where
        c = succ (messageCount rs)

data ReceiveState = ReceiveState
  { messageCount       :: !Int
  , messageWeightedSum :: !Double
  } deriving (Eq, Show, Read)

initReceiveState :: ReceiveState
initReceiveState = ReceiveState 0 0

--------------------------------------------------------------------------------

sendProcess :: Options -> Process ()
sendProcess opts =
  evalStateT go (initSendState opts)
  `catchExit`
  (\_ ExitSend -> say "No longer sending messages"
                  >> nsend messageReceivingService (Nothing :: Maybe Message))
  -- When we have to exit, indicate this to the receiving thread

  where
    go :: StateT SendState Process ()
    go = forever $ do
      g <- gets seed
      let (msg, g') = genMessage g
      say ("Sending message of " ++ show msg)
      sendToPeers (Just msg)
      modify (\ss -> ss { seed = g' })

    -- random will return a function in [0,1), but we want (0,1]; as
    -- such we need to convert this.
    genMessage :: StdGen -> (Double, StdGen)
    genMessage = first (abs . (1-)) . random

    sendToPeers msg = liftP (P2P.getCapable messageReceivingService)
                      >>= mapM_ (forward (wrapMessage msg))

newtype SendState = SendState
  { seed :: StdGen
  } deriving (Show)

initSendState :: Options -> SendState
initSendState = SendState . mkStdGen . withSeed
