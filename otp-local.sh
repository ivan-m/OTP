#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

function run_otp {
    stack exec otp -- "$@"
}

function to_stderr {
    >&2 "$@"
}

# Ensure it's built
to_stderr stack build

HOST="127.0.0.1"

OUTPUT_PREFIX="otp-local-out"
ERR_PREFIX="otp-local-err"

################################################################################

if [ $# -eq 0 ] || { [ $# -ge 1 ] && [ "$1" == "--help" ]; }; then
    to_stderr run_otp "--help"
    to_stderr echo "(Please note that node details are provided by `basename $0`)"
    exit 0
fi

ARGS=$*

rm -f $OUTPUT_PREFIX.*
rm -f $ERR_PREFIX.*

declare -a processed_nodes=()

declare -a node_ports=( 10000 10001 10002 10003 )

declare -a processes=()

WAIT_FOR=""

for i in ${!node_ports[*]}; do
    run_otp ${ARGS} "${HOST}" "${node_ports[$i]}" "${processed_nodes[@]}" \
        > "${OUTPUT_PREFIX}.${i}" \
        2> "${ERR_PREFIX}.${i}" &
    processes[i]="$!"
    processed_nodes[i]="${HOST}:${node_ports[$i]}"
done

# Waiting for jobs to complete
for i in ${!processes[*]}; do
    wait ${processes[i]}
done

# Output results
cat $OUTPUT_PREFIX.*
